# Contributor: Grigory Kirillov <txgk@bk.ru>
# Maintainer: Grigory Kirillov <txgk@bk.ru>
pkgname=simdjson
pkgver=2.0.1
pkgrel=0
pkgdesc="Parsing gigabytes of JSON per second"
url="https://simdjson.org"
arch="all"
license="Apache-2.0"
# tests take a very long time to compile and require downloading other JSON parsers
options="!check"
makedepends="cmake samurai"
subpackages="$pkgname-static $pkgname-dev"
source="https://github.com/simdjson/simdjson/archive/v$pkgver/simdjson-$pkgver.tar.gz"

build() {
	cmake -B builddir -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DBUILD_SHARED_LIBS=ON
	cmake -B builddir-static -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release
	cmake --build builddir
	cmake --build builddir-static
}

package() {
	DESTDIR="$pkgdir" cmake --install builddir
	DESTDIR="$pkgdir" cmake --install builddir-static
}

sha512sums="
09c72669fbd73e92e54f4c6e21e59533dae60db655de087264be4f0a6dd305e63961ca55973d47081a4310c2bc1afa7304d1528cae65fe5b237d659604c7d716  simdjson-2.0.1.tar.gz
"
