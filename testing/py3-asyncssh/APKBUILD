# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-asyncssh
_relname=${pkgname#"py3-"}
pkgver=2.10.1
pkgrel=0
pkgdesc="provides an asynchronous client and server implementation of the SSHv2 protocol on top of the Python 3.6+ asyncio framework"
url="https://asyncssh.readthedocs.io/en/latest/"
arch="noarch !s390x" # tests hang
license="EPL-2.0"
depends="python3 py3-cryptography py3-typing-extensions"
makedepends="py3-build py3-installer py3-setuptools py3-wheel"
checkdepends="py3-bcrypt py3-openssl py3-pytest"
source="https://files.pythonhosted.org/packages/source/a/asyncssh/asyncssh-$pkgver.tar.gz"
builddir="$srcdir/$_relname-$pkgver"

build() {
	python3 -m build --no-isolation --wheel
}

check() {
	python3 -m installer -d testenv \
		dist/$_relname-$pkgver-py3-none-any.whl
	local sitedir="$(python3 -c 'import site;print(site.getsitepackages()[0])')"
	# deselected tests have x11 forwarding error and openssl too old error
	PYTHONPATH="$PWD/testenv/$sitedir" python3 -m pytest --deselect tests/test_x11.py \
		--deselect tests/test_connection.py \
		--deselect tests/test_encryption.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/$_relname-$pkgver-py3-none-any.whl
}

sha512sums="
adaf68f2e4209e9bf91c2018298a281dab490d723bda9d6f7db892aa43e796e38ff904cf39f56c8b545439ca71c89b419e53521f323cb753cf7a2290beb04e12  asyncssh-2.10.1.tar.gz
"
