# Contributor: nibon7 <nibon7@163.com>
# Maintainer: nibon7 <nibon7@163.com>
pkgname=nushell
pkgver=0.63.1
pkgrel=2
pkgdesc="A new type of shell"
url="https://www.nushell.sh"
# s390x/riscv64 rust/cargo
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"
license="MIT"
makedepends="cargo pkgconf openssl-dev libx11-dev libxcb-dev libgit2-dev"
subpackages="$pkgname-plugins:_plugins"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
source="$pkgname-$pkgver.tar.gz::https://github.com/nushell/nushell/archive/$pkgver.tar.gz"

# Reduce size of nu binary from 22.7 -> 13.8 MiB (on x86_64 with default feature).
export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="fat"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="s"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --workspace --release --frozen
}

check() {
	cargo test --workspace --frozen
}

package() {
	find target/release \
		-maxdepth 1 \
		-executable \
		-type f \
		-name "nu*" \
		-exec install -Dm755 '{}' -t "$pkgdir"/usr/bin/ \;
}

_plugins() {
	pkgdesc="Nushell plugins"
	depends="nushell"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/nu_plugin_* "$subpkgdir"/usr/bin/
}

sha512sums="23be244b6b33ea1977fd690def7ec22e0971bdc1bc7e5d5537a3fc5af7b7ba0324ed7a016f9664f8ebaeaa94fdaa04d2d1053b5733342878095f3b803f2b6d58  nushell-0.63.1.tar.gz"
