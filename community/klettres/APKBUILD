# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=klettres
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> knewstuff
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/klettres"
pkgdesc="Learn the Alphabet"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	knewstuff-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/klettres-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c6827e36f22a10c278b8f38f8342d2a4c5b454a83b22dcbc2b84cc2394a86fcc247d8505a003ca93258f3aa2016ac224477c4be98f33c6b467616f9cc8102d52  klettres-22.04.1.tar.xz
"
