# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkcddb
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="KDE CDDB library"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcodecs-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	libmusicbrainz-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkcddb-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="net" # Required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	cd build
	# musicbrainztest-severaldiscs, asynccddblookuptest and synccddblookuptest are broken
	# asynchttplookuptest fails to setup dbus
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(musicbrainztest-severaldiscs|asynchttplookuptest|asynccddblookuptest|synccddblookuptest)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c6d4f75878d26c8809fa2fbd9cbe9903fe2739cbd282cdf336f81d5b481bb444f68d1fa59386c15bb3778eaf535b7fb7b0f617dff95b0ecf8d2ffe147bd6e46b  libkcddb-22.04.1.tar.xz
"
