# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kipi-plugins
pkgver=22.04.1
pkgrel=0
pkgdesc="A collection of plugins extending the KDE graphics and image applications"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://userbase.kde.org/KIPI"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkipi-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kipi-plugins-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
583ecc3c2e34e3e9f541e7ba9297831ccb3781b3acec0bb08c33df23f7c9d6e8f204d75b8895066318c4d2e0c22aaa605c48798ea3c06aa5783e9109fec34a89  kipi-plugins-22.04.1.tar.xz
"
