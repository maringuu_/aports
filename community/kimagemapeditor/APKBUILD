# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kimagemapeditor
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/development/org.kde.kimagemapeditor"
pkgdesc="An editor of image maps embedded inside HTML files, based on the <map> tag"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	khtml-dev
	ki18n-dev
	kiconthemes-dev
	kparts-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kimagemapeditor-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9e1a041311a9a4f5c8aa81fc3c9752ab83cae12d8758de3ac2749dad9c23215d9102d9e0c5ee8f65c9036a2828d299f7de168f65a8d14f2a61319faefc3da788  kimagemapeditor-22.04.1.tar.xz
"
