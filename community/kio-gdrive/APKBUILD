# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kio-gdrive
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> libkgapi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KIO_GDrive"
pkgdesc="KIO Slave to access Google Drive"
license="GPL-2.0-or-later"
depends="
	kaccounts-providers
	signon-plugin-oauth2
	signon-ui
	"
makedepends="
	extra-cmake-modules
	intltool
	kaccounts-integration-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	libkgapi-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service//$pkgver/src/kio-gdrive-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e67c7a7d517947fb0f1f54a6f3e4a4ee5fa5a7713936b0615272127edd553f89210ecbd74b564962966f3c5047ee1581d1d2ee6078584cb6bde58033a8c57470  kio-gdrive-22.04.1.tar.xz
"
