# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=15.4.2
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
		$pkgname-aile
		$pkgname-etoile
	"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/iosevka/iosevka-etoile.ttc
}

sha512sums="
619fcaf344a569a02cdb079a84d1a62ee56fedfb5e013a8805c25683f188af870cfd9b5b415d25ef3ffeddcfd2e913be5727004ea18ce2273abdc2c487258aaf  super-ttc-iosevka-15.4.2.zip
2e6d58a350618f40e1a2dce8d5410985f3c6d6df163037455f2ad7d2f8a1a0d5ba9ac9369f1fb02507603c58298a3ff1a72e2be9d1b1b346f4bd743dfa5e4857  super-ttc-iosevka-slab-15.4.2.zip
df5a3a07ade0681713365c1691330c4ced5aecc41ca42cbb721a4d02e6ef1a46641784118390f25fa9db4f333156d79de88c5916bb38bf9984bbf3fcffd01116  super-ttc-iosevka-curly-15.4.2.zip
0b1166381729ff2989e7f575e9a8582c420840f8fc857f2b56eb6b6fe888ed8fc3a3f6955809f0105555b15de14c08ab106da32252ea07dd2836cc79872cdf11  super-ttc-iosevka-curly-slab-15.4.2.zip
635683e71e0fa793c0a2b896b80aae64dfaa860fb9347d86a33483bed01e3e1298ce2041caef742679d2ec7d5e73dfb33b1c8fde39aa1391f44266af80190019  super-ttc-iosevka-aile-15.4.2.zip
bacd9deddd8b45ca56ca93549a9795b92987771b9c1b274c5e3fb1c43009da1b8e4e8d6e0f9e01f7950e3c84e7f7652386286098bcc9d339710538e4f9ce7ff7  super-ttc-iosevka-etoile-15.4.2.zip
"
