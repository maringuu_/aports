# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpat
pkgver=22.04.1
pkgrel=0
pkgdesc="KPatience offers a selection of solitaire card games"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/games/kpat/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	black-hole-solver-dev
	extra-cmake-modules
	freecell-solver-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpat-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
# Broken tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
81f8bc1b86161a7282e6a110b433f721383f9fc654a39dd7feac2cd476df2f960e57822a1f03bd0a52d3c0decef53b6436b07a106b67c05957a26ddbc8a3e24e  kpat-22.04.1.tar.xz
"
