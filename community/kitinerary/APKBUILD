# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kitinerary
pkgver=22.04.1
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# ppc64le FTBFS
arch="all !armhf !ppc64le"
url="https://kontact.kde.org/"
pkgdesc="Data model and extraction system for travel reservation information"
license="LGPL-2.0-or-later"
depends_dev="
	kcalendarcore-dev
	kcontacts-dev
	kmime-dev
	kpkpass-dev
	libphonenumber-dev
	samurai
	zxing-cpp-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	ki18n-dev
	libxml2-dev
	poppler-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	shared-mime-info
	zlib-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kitinerary-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# jsonlddocumenttest, mergeutiltest, airportdbtest, pkpassextractortest,
	# postprocessortest, calendarhandlertest, extractortest and knowledgedbtest are broken
	local skipped_tests="(jsonlddocument"
	local tests="
		mergeutil
		airportdb
		pkpassextractor
		postprocessor
		calendarhandler
		extractor
		knowledgedb
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d3229a107713dbf16aca48cb85a52787b5d6891b55eff21106196e5f859e6ae2fd036bccf21110a02042f04375a821025838026e94b99edcac2120988de08836  kitinerary-22.04.1.tar.xz
"
