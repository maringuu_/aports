# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Eric Timmons <etimmons@alum.mit.edu>
# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=sbcl
pkgver=2.2.5
pkgrel=0
pkgdesc="Steel Bank Common Lisp"
url="http://www.sbcl.org/"
# sbcl 2.2.5 has a "memory fault" on x86
arch="x86_64 armv7 aarch64"
license="custom"
options="!check"
checkdepends="ed"
makedepends="
	ecl-dev>=21.2.1-r2
	gmp-dev
	gc-dev
	libffi-dev
	linux-headers
	zlib-dev
	emacs-nox
	mpfr-dev
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.bz2::https://prdownloads.sourceforge.net/sbcl/sbcl-$pkgver-source.tar.bz2
	march-armv5-removed.patch
	"

build() {
	GNUMAKE=make ./make.sh "ecl --norc" \
		--prefix=/usr --fancy \
		--with-sb-test \
		--with-sb-unicode \
		--with-sb-linkable-runtime \
		--with-sb-dynamic-core \
		--with-sb-core-compression \
		--dynamic-space-size=3072
}

package() {
	INSTALL_ROOT="$pkgdir/usr" \
		LIB_DIR="/usr/lib" \
		DOC_DIR="$pkgdir/usr/share/doc/$pkgname" \
		sh install.sh

	install -Dm644 COPYING \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE.txt

	rmdir "$pkgdir"/usr/share/doc/$pkgname/html \
		"$pkgdir"/usr/share/info 2>/dev/null || true
}

sha512sums="
9d581535348bb709ca1f7edbe5ad92f079fd1cbc9a462524220fb229bd78770f7855c0b1a8641b990a1d663fb559edc0c2ca0f029281d4b0c80a5917b45d7b72  sbcl-2.2.5.tar.bz2
253ad5d87cd7c656f1d011ec3519950f4dbffc9fb9a4a8e12f61c7cc51915ec4f3bf2fc47024613eeb5741851f07047ad3f4237b74ecc2d8bb93cc0059de6092  march-armv5-removed.patch
"
