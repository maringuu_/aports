# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=calendarsupport
pkgver=22.04.1
pkgrel=0
pkgdesc="Library providing calendar support"
# armhf blocked by extra-cmake-modules
# ppc64le blocked by akonadi-calendar
# s390x, and riscv64 blocked by polkit -> kio
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org"
license="GPL-2.0-or-later AND Qt-GPL-exception-1.0 AND LGPL-2.0-or-later"
depends_dev="
	akonadi-calendar-dev
	akonadi-dev
	akonadi-mime-dev
	akonadi-notes-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kidentitymanagement-dev
	kio-dev
	kmime-dev
	pimcommon-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/calendarsupport-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
27aa3920cf63ee6ce2e5c668aac2f72efa9574fe805c8c72a049ce603f5174072277867e31431babbb9933eebb41f0231b6d44544ce2a55212b9b970e156e15a  calendarsupport-22.04.1.tar.xz
"
