# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkleo
pkgver=22.04.1
pkgrel=0
pkgdesc="KDE PIM cryptographic library"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kpimtextedit
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org"
license="GPL-2.0-or-later"
# TODO: Maybe replace gnupg with specific gnupg subpackages.
depends="gnupg"
makedepends="
	boost-dev
	extra-cmake-modules
	gpgme-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kitemmodels-dev
	kpimtextedit-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkleo-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "newkeyapprovaldialogtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
284de354353f977f0e19acf0a199ddf329e1325492c31072a4eb46655e2cccb8e1a7df8b036bd2c3a1d207200162287bceefe20b7179320504dcc4d488e39a65  libkleo-22.04.1.tar.xz
"
