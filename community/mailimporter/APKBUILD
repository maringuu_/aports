# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=mailimporter
pkgver=22.04.1
pkgrel=0
pkgdesc="KDE PIM library providing support for mail applications"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> pimcommon
# ppc64le blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kmime-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/mailimporter-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
15d97a5c1db54b647fd5058de35849d75e1451a940b048eea43fce3078cd946b4385e9d90c45170b967494c147f40214db516200d85adc1f3003a5a3641f6713  mailimporter-22.04.1.tar.xz
"
