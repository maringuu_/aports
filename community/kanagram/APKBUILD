# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kanagram
pkgver=22.04.1
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/kanagram/"
pkgdesc="Letter Order Game"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	libkeduvocdocument-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	sonnet-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kanagram-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0882aaadc6306f9c7289f5ab583964ae0ad8758957c360a6eee3fbcd2ffc8cddb5978353a2bb0daae141b4e2da8778b9d4693bebf2dc1d7740fcbd090aeb676c  kanagram-22.04.1.tar.xz
"
