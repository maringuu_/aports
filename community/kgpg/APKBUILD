# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kgpg
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
# ppc64le blocked by qt5-qtwebengine -> kaccounts-integration
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://kde.org/applications/utilities/org.kde.kgpg"
pkgdesc="A simple interface for GnuPG, a powerful encryption utility"
license="GPL-2.0-or-later"
makedepends="
	akonadi-contacts-dev
	extra-cmake-modules
	gpgme-dev
	karchive-dev
	kcodecs-dev
	kcontacts-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgpg-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kgpg-import fails too often
	# kgpg-encrypt and kgpg-export are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "kgpg-(import|encrypt|export)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cdef93d8f7785cea2fdba589e3e1935804ea9b6821834066144e77c1e8e328c75ef311f8ab3d2915832d651187f67df5e522232631ffc03dd0d33c4cac6abfcc  kgpg-22.04.1.tar.xz
"
